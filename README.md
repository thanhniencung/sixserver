# Readme
Server Pes6 online dựa trên Bản Ikec có giao diện web.
Link gốc: https://github.com/IkeC/evo-league
## Hướng dẫn cài:
### Yêu cầu hệ thống:
* Server: 2Gb ram / dual core CPU
* OS: Centos 6 X64
* Python: 2.6
* Mysql: 5.5/5.6
* Apache: 2.2/2.4
* PHP: 5.5/5.6 Với short_open_tag = On trong php.ini
* Quyền root.
### Cài đặt
1. Tải file install.sh về thư mục /tmp
2. Chạy lênh: sh install.sh
3. Kiểm tra: nestat -tpuln
4. Vào /var/www/html/ sửa file config.php : sửa URL của site.
### Chơi game:
1. Tài khoản quản trị: Username: admin Password: changeme
2. Đăng ký tài khoản và chơi. Điền serial vào ô Serial Pes6
3. Tải file host.zip  về máy và giải nén ra file hosts; Sửa file hosts đổi IP thành IP của server vừa cài. sau đó chép đè vào c:\windows\system32\drivers\etc\
4. Kết nối từ Pes6 đăng nhập dưới dạng: username-password
5. Kêu gọi bạn bè đăng ký chơi cùng.