#!/bin/bash
# For: Centos 6.x
# Script to add repository to update & Maintanence Centos 6 after EOL
# Sript Install sixserver: PES6VN
# Write by thanhniencung
# Update: 18 Jun 2021

# Database Infomation ########
sqlrootpassword="D048LQ7CiH4f"
sqlusername="pes6vn"
datbasename="pes6vn"
databasepwd="y3dyOI37Cpu1"
##############################

mkdir -p /tmp/fix
cd /tmp/fix
rm -fr /tmp/fix/*.*

echo "Fix to Use the CentOS Vault repository"
curl https://www.getpagespeed.com/files/centos6-eol.repo --output /etc/yum.repos.d/CentOS-Base.repo

# Upgrade Centos:
yum upgrade -y

echo "Fixing EPEL repository"
#curl https://www.getpagespeed.com/files/centos6-epel-eol.repo --output /etc/yum.repos.d/epel.repo
curl -O https://fedora-archive.ip-connect.info/epel/6/x86_64/epel-release-6-8.noarch.rpm
rpm -Uvh epel-release-6-8.noarch.rpm

echo  "Fixing SCLO repositories"
yum -y install centos-release-scl
curl https://www.getpagespeed.com/files/centos6-scl-eol.repo --output /etc/yum.repos.d/CentOS-SCLo-scl.repo
curl https://www.getpagespeed.com/files/centos6-scl-rh-eol.repo --output /etc/yum.repos.d/CentOS-SCLo-scl-rh.repo

rm -fr *
cd /tmp/
yum groupinstall "Development Tools" -y
yum install openssl-devel python-devel mysql-devel libyaml git -y  

git clone https://bitbucket.org/thanhniencung/sixserver/src/master/ /tmp/pes
cd /tmp/pes

echo
echo "complete install lib"
cd /tmp/pes/python

tar -xzvf setuptools-19.2.tar.gz
cd setuptools-19.2
python setup.py install
cd ..
tar -zxvf pip-7.1.2.tar.gz
cd pip-7.1.2
python setup.py install
cd ..
tar -zxvf pyOpenSSL-0.13.tar.gz
cd pyOpenSSL-0.13
python setup.py install
cd ..
tar -xzvf pycrypto-2.3.tar.gz
cd pycrypto-2.3
python setup.py install
cd ..
tar -xzvf MySQL-python-1.2.3.tar.gz
cd MySQL-python-1.2.3
python setup.py install
cd ..
tar -xzvf PyYAML-3.10.tar.gz
cd PyYAML-3.10
python setup.py install
cd ..
tar -xzvf zope.interface-3.8.0.tar.gz
cd zope.interface-3.8.0
python setup.py install
cd ..
tar -xvf Twisted-10.2.0.tar
cd Twisted-10.2.0
python setup.py install

cd /tmp/pes/sql

yum -y install mysql-server
service mysqld start
chkconfig mysqld on

#################################
# set root Password for MySQL####
# please set your password ######
#################################

config=".my.cnf.$$"
command=".mysql.$$"

trap "interrupt" 1 2 3 6 15

mysqladmin -u root password $sqlrootpassword
rootpass=
echo_n=
echo_c=
basedir=
bindir=

parse_arg()
{
  echo "$1" | sed -e 's/^[^=]*=//'
}

parse_arguments()
{
  # We only need to pass arguments through to the server if we don't
  # handle them here.  So, we collect unrecognized options (passed on
  # the command line) into the args variable.
  pick_args=
  if test "$1" = PICK-ARGS-FROM-ARGV
  then
    pick_args=1
    shift
  fi

  for arg
  do
    case "$arg" in
      --basedir=*) basedir=`parse_arg "$arg"` ;;
      --no-defaults|--defaults-file=*|--defaults-extra-file=*)
        defaults="$arg" ;;
      *)
        if test -n "$pick_args"
        then
          # This sed command makes sure that any special chars are quoted,
          # so the arg gets passed exactly to the server.
          # XXX: This is broken; true fix requires using eval and proper
          # quoting of every single arg ($basedir, $ldata, etc.)
          #args="$args "`echo "$arg" | sed -e 's,\([^a-zA-Z0-9_.-]\),\\\\\1,g'`
          args="$args $arg"
        fi
        ;;
    esac
  done
}

# Try to find a specific file within --basedir which can either be a binary
# release or installed source directory and return the path.
find_in_basedir()
{
  return_dir=0
  found=0
  case "$1" in
    --dir)
      return_dir=1; shift
      ;;
  esac

  file=$1; shift

  for dir in "$@"
  do
    if test -f "$basedir/$dir/$file"
    then
      found=1
      if test $return_dir -eq 1
      then
        echo "$basedir/$dir"
      else
        echo "$basedir/$dir/$file"
      fi
      break
    fi
  done

  if test $found -eq 0
  then
      # Test if command is in PATH
      $file --no-defaults --version > /dev/null 2>&1
      status=$?
      if test $status -eq 0
      then
        echo $file
      fi
  fi
}

cannot_find_file()
{
  echo
  echo "FATAL ERROR: Could not find $1"

  shift
  if test $# -ne 0
  then
    echo
    echo "The following directories were searched:"
    echo
    for dir in "$@"
    do
      echo "    $dir"
    done
  fi

  echo
  echo "If you compiled from source, you need to run 'make install' to"
  echo "copy the software into the correct location ready for operation."
  echo
  echo "If you are using a binary release, you must either be at the top"
  echo "level of the extracted archive, or pass the --basedir option"
  echo "pointing to that location."
  echo
}

# Ok, let's go.  We first need to parse arguments which are required by
# my_print_defaults so that we can execute it first, then later re-parse
# the command line to add any extra bits that we need.
parse_arguments PICK-ARGS-FROM-ARGV "$@"

#
# We can now find my_print_defaults.  This script supports:
#
#   --srcdir=path pointing to compiled source tree
#   --basedir=path pointing to installed binary location
#
# or default to compiled-in locations.
#

if test -n "$basedir"
then
  print_defaults=`find_in_basedir my_print_defaults bin extra`
  echo "print: $print_defaults"
  if test -z "$print_defaults"
  then
    cannot_find_file my_print_defaults $basedir/bin $basedir/extra
    exit 1
  fi
else
  print_defaults="/usr/bin/my_print_defaults"
fi

if test ! -x "$print_defaults"
then
  cannot_find_file "$print_defaults"
  exit 1
fi

# Now we can get arguments from the group [client] and [client-server]
# in the my.cfg file, then re-run to merge with command line arguments.
parse_arguments `$print_defaults $defaults client client-server client-MySQL`
parse_arguments PICK-ARGS-FROM-ARGV "$@"

# Configure paths to support files
if test -n "$basedir"
then
  bindir="$basedir/bin"
elif test -f "./bin/mysql"
  then
  bindir="./bin"
else
  bindir="/usr/bin"
fi

mysql_command=`find_in_basedir mysql $bindir`
if test -z "$mysql_command"
then
  cannot_find_file mysql $bindir
  exit 1
fi

set_echo_compat() {
    case `echo "testing\c"`,`echo -n testing` in
	*c*,-n*) echo_n=   echo_c=     ;;
	*c*,*)   echo_n=-n echo_c=     ;;
	*)       echo_n=   echo_c='\c' ;;
    esac
}

validate_reply () {
    ret=0
    if [ -z "$1" ]; then
	reply=y
	return $ret
    fi
    case $1 in
        y|Y|yes|Yes|YES) reply=y ;;
        n|N|no|No|NO)    reply=n ;;
        *) ret=1 ;;
    esac
    return $ret
}

prepare() {
    touch $config $command
    chmod 600 $config $command
}

do_query() {
    echo "$1" >$command
    #sed 's,^,> ,' < $command  # Debugging
    $mysql_command --defaults-file=$config <$command
    return $?
}

# Simple escape mechanism (\-escape any ' and \), suitable for two contexts:
# - single-quoted SQL strings
# - single-quoted option values on the right hand side of = in my.cnf
#
# These two contexts don't handle escapes identically.  SQL strings allow
# quoting any character (\C => C, for any C), but my.cnf parsing allows
# quoting only \, ' or ".  For example, password='a\b' quotes a 3-character
# string in my.cnf, but a 2-character string in SQL.
#
# This simple escape works correctly in both places.
basic_single_escape () {
    # The quoting on this sed command is a bit complex.  Single-quoted strings
    # don't allow *any* escape mechanism, so they cannot contain a single
    # quote.  The string sed gets (as argv[1]) is:  s/\(['\]\)/\\\1/g
    #
    # Inside a character class, \ and ' are not special, so the ['\] character
    # class is balanced and contains two characters.
    echo "$1" | sed 's/\(['"'"'\]\)/\\\1/g'
}

make_config() {
    echo "# mysql_secure_installation config file" >$config
    echo "[mysql]" >>$config
    echo "user=root" >>$config
    esc_pass=`basic_single_escape "$rootpass"`
    echo "password='$esc_pass'" >>$config
    #sed 's,^,> ,' < $config  # Debugging
}

get_root_password() {
    status=1
    rootpass=$sqlrootpassword
	make_config
	do_query ""
	status=$?
    
    echo "OK, successfully used password, moving on..."
    echo
}

set_root_password() {
        return 0
}

remove_anonymous_users() {
    do_query "DELETE FROM mysql.user WHERE User='';"
    if [ $? -eq 0 ]; then
	echo " ... Success!"
    else
	echo " ... Failed!"
	clean_and_exit
    fi

    return 0
}

remove_remote_root() {
    do_query "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
    if [ $? -eq 0 ]; then
	echo " ... Success!"
    else
	echo " ... Failed!"
    fi
}

remove_test_database() {
    echo " - Dropping test database..."
    do_query "DROP DATABASE IF EXISTS test;"
    if [ $? -eq 0 ]; then
	echo " ... Success!"
    else
	echo " ... Failed!  Not critical, keep moving..."
    fi

    echo " - Removing privileges on test database..."
    do_query "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%'"
    if [ $? -eq 0 ]; then
	echo " ... Success!"
    else
	echo " ... Failed!  Not critical, keep moving..."
    fi

    return 0
}

reload_privilege_tables() {
    do_query "FLUSH PRIVILEGES;"
    if [ $? -eq 0 ]; then
	echo " ... Success!"
	return 0
    else
	echo " ... Failed!"
	return 1
    fi
}

interrupt() {
    echo
    echo "Aborting!"
    echo
    cleanup
    stty echo
    exit 1
}

cleanup() {
    echo "Cleaning up..."
    rm -f $config $command
}

# Remove the files before exiting.
clean_and_exit() {
	cleanup
	exit 1
}

# The actual script starts here

prepare
set_echo_compat

echo
echo "NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MySQL"
echo "      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!"
echo
echo "In order to log into MySQL to secure it, we'll need the current"
echo "password for the root user.  If you've just installed MySQL, and"
echo "you haven't set the root password yet, the password will be blank,"
echo "so you should just press enter here."
echo

get_root_password



#
# Remove anonymous users
#

echo "By default, a MySQL installation has an anonymous user, allowing anyone"
echo "to log into MySQL without having to have a user account created for"
echo "them.  This is intended only for testing, and to make the installation"
echo "go a bit smoother.  You should remove them before moving into a"
echo "production environment."
echo

remove_anonymous_users

echo


#
# Disallow remote root login
#

echo "Normally, root should only be allowed to connect from 'localhost'.  This"
echo "ensures that someone cannot guess at the root password from the network."
echo

remove_remote_root


#
# Remove test database
#

echo "By default, MySQL comes with a database named 'test' that anyone can"
echo "access.  This is also intended only for testing, and should be removed"
echo "before moving into a production environment."
echo


    remove_test_database

#
# Reload privilege tables
#

echo "Reloading the privilege tables will ensure that all changes made so far"
echo "will take effect immediately."
echo
reload_privilege_tables


cleanup
echo
echo "All done!  If you've completed all of the above steps, your MySQL"
echo "installation should now be secure."
echo
echo "Thanks for using MySQL!"


###########

# Create Datbase for Sixserver

mysql -u root -p$sqlrootpassword <<EOF
CREATE DATABASE $datbasename;
GRANT ALL PRIVILEGES ON $datbasename.* TO "$sqlusername"@"localhost" IDENTIFIED BY "$databasepwd";
FLUSH PRIVILEGES;
EOF
#############################
#import init database
mysql -u$sqlusername -p$databasepwd $datbasename < dbinit.sql

#############################
cd /tmp/pes
yum install -y httpd
chkconfig httpd on
yum install yum-utils -y


rpm -Uvh https://rpms.remirepo.net/enterprise/remi-release-6.rpm
yum-config-manager --enable "remi-php55" >> /dev/null 2>&1
yum install -y php php-cli php-mysqlnd php-mysqli php-xml php-gd php-mcrypt php-bcmath php-pear php-pecl-jsonc php-pecl-zip 


sed -i 's/^max_execution_time.*/max_execution_time=600/' /etc/php.ini
sed -i 's/^max_input_time.*/max_input_time=600/' /etc/php.ini
sed -i 's/^post_max_size.*/post_max_size=128M/' /etc/php.ini
sed -i 's/^short_open_tag.*/short_open_tag = On/' /etc/php.ini
sed -i "s/^\;date.timezone.*/date.timezone=\'Asia\/Bangkok\'/" /etc/php.ini
echo "ServerName 127.0.0.1" >> /etc/httpd/conf/httpd.conf
service httpd restart
rm -fr /var/www/html/*
cp -r html/* /var/www/html/
chown -R apache:apache /var/www/html
# install sixserver
cd /tmp/pes
cp -r sixserver /opt/
cd /opt/sixserver

mkdir -p /var/log/sixserver

cp service.sh /etc/init.d/sixserver
chmod +x /etc/init.d/sixserver
service sixserver start
chkconfig sixserver on

# Open firewall
iptables -I INPUT -m udp -p udp --dport 3478 -j ACCEPT
iptables -I INPUT -m tcp -p tcp --dport 3479 -j ACCEPT
iptables -I INPUT -m tcp -p tcp --dport 3478 -j ACCEPT
iptables -I INPUT -m tcp -p tcp --dport 5766 -j ACCEPT
iptables -I INPUT -m tcp -p tcp --dport 10881 -j ACCEPT
iptables -I INPUT -m tcp -p tcp --dport 20200 -j ACCEPT
iptables -I INPUT -m tcp -p tcp --dport 20201 -j ACCEPT
iptables -I INPUT -m tcp -p tcp --dport 20202 -j ACCEPT
iptables -I INPUT -m tcp -p tcp --dport 20203 -j ACCEPT
iptables -I INPUT -m tcp -p tcp --dport 8191 -j ACCEPT
iptables -I INPUT -m tcp -p tcp --dport 80 -j ACCEPT
service iptables save
#######################
echo


echo "done"

