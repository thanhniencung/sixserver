<?php


// the index page showing the last 10 games, top 10 players from standings,
// the latest news entry and the last 10 players that joined and were approved

session_start();
$token = md5(uniqid(rand(), true));
$_SESSION['token'] = $token;

$page = "home";
$subpage = "";
require ('variables.php');
require ('variablesdb.php');
require ('functions.php');
require ('top.php');

$boxheight = "225";
$rowspan2 = 'rowspan="2"';

$box1Img = "8.jpg";
$box1Align = "left bottom";
$box2Img = "2.jpg";
$box2Align = "left bottom";
$box3Img = "5.jpg";
$box3Align = "left center";
$box4Img = "9.jpg";
$box4Align = "left center";

$sql = "SELECT begindate, enddate from $seasonstable where season = '$season'";
$result = mysql_query($sql);
$row = mysql_fetch_array($result);
$begindate = $row['begindate'];
$enddate = $row['enddate'];

$timezone = date("T");
if (strlen($timezone) > 4) {
	$timezone = "";
}

$left = 'Welcome to '.$leaguename.'!'.'&nbsp;&nbsp;'.'<span class="grey-small" style="font-weight:normal">evo time is '.date("m/d h:i a").' '.$timezone.'</span>';
$right = '<span class="black-small">Ladder Season '.$season.'&nbsp;('.$begindate.' - '.$enddate.')</span>';

$sql = "SELECT * FROM $newstable ORDER BY news_id DESC LIMIT 0, 1";
$result = mysql_query($sql);
$row = mysql_fetch_array($result);
$news = $row["news"];
$news = nl2br($news);
$news = SmileyConvert($news, $directory);
$date = $row["date"];
$title = $row["title"];
$user = $row["user"];
$parsedTime = strtotime($date);

/*
if ((time() - $parsedTime) < 60 * 60 * 24 * 3) {
	$isNew = true;
} else {
	$isNew = false;
}*/
$isNew = true;

?>

<?= getOuterBoxTop($left, $right) ?>

<!-- <link rel="stylesheet" type="text/css" href="/wtag/css/main-style.css" /> -->
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="wtag/css/ie-style.css" />
<![endif]-->
<!--
<script type="text/javascript" src="/wtag/js/dom-drag.js"></script>
<script type="text/javascript" src="/wtag/js/scroll.js"></script>
<script type="text/javascript" src="/wtag/js/ajax.js"></script>
-->
<table width="100%">
	<tr>
		<td width="50%" valign="top"><!-- games --> <?
		$gamesLinksArray[] = array ('./games.php', 'show_games', 'show all games');
		echo getBoxTopImg("Kết Quả Các Trận", $boxheight, false, $gamesLinksArray, $box1Img, $box1Align);
		?>
		<table class="gamesbox">
		<?

		$sql = "SELECT sm.id, UNIX_TIMESTAMP(sm.played_on) as played_on, sm.score_home, sm.score_away, ".
            "st1.ladderTeamId as ladderTeamHome, st2.ladderTeamId as ladderTeamAway ". 
            "FROM six_matches sm ".
            "LEFT JOIN six_patches sp1 ON sm.hashHome=sp1.hash ".
            "LEFT JOIN six_patches sp2 ON sm.hashAway=sp2.hash ".
            "LEFT JOIN six_teams st1 ON (st1.sixTeamId=sm.team_id_home AND st1.patchId=sp1.id) ".
            "LEFT JOIN six_teams st2 ON (st2.sixTeamId=sm.team_id_away AND st2.patchId=sp2.id) ".
            "ORDER BY sm.id DESC LIMIT 0, 10";
            
//    $log = new KLogger('/var/www/yoursite/http/log/general/', KLogger::INFO);	
//    $log->logInfo('xx sql='.$sql);

		$result = mysql_query($sql);
		$compteur = 1;
		while (10 >= $compteur) {
			$row = mysql_fetch_array($result);
			if (!empty ($row)) {
				$id = $row['id'];
				$gamedate = formatTime($row['played_on']);
				$score_home = $row['score_home'];
				$score_away = $row['score_away'];
        $ladderTeamHome = $row['ladderTeamHome'];
        $ladderTeamAway = $row['ladderTeamAway'];

				$sql2 = "SELECT weblm_players.name, weblm_players.nationality, six_profiles.name AS profileName FROM weblm_players " .
				"LEFT JOIN six_profiles ON six_profiles.user_id = weblm_players.player_id " .
				"LEFT JOIN six_matches_played ON six_matches_played.profile_id = six_profiles.id " .
				"WHERE six_matches_played.match_id=$id " .
				"AND six_matches_played.home=1";
				
				$result2 = mysql_query($sql2);
				$row2 = mysql_fetch_array($result2);
				$player_home = $row2['name'];
				$nationalityHome = $row2['nationality'];
				$profileNameHome = $row2['profileName'];
				
        $player_home2 = "";
        $nationalityHome2 = "";
        $profileNameHome2 = "";

        $player_home3 = "";
        $nationalityHome3 = "";
        $profileNameHome3 = "";

        $row2 = mysql_fetch_array($result2);
				if (!empty($row2)) {
					$player_home2 = $row2['name'];
					$nationalityHome2 = $row2['nationality'];
					$profileNameHome2 = $row2['profileName'];
          $row2 = mysql_fetch_array($result2);
          if (!empty($row2)) {
            $player_home3 = $row2['name'];
            $nationalityHome3 = $row2['nationality'];
            $profileNameHome3 = $row2['profileName'];
          } 
				} 
				
				$sql2 = "SELECT weblm_players.name, weblm_players.nationality, six_profiles.name AS profileName FROM weblm_players " .
				"LEFT JOIN six_profiles ON six_profiles.user_id = weblm_players.player_id " .
				"LEFT JOIN six_matches_played ON six_matches_played.profile_id = six_profiles.id " .
				"WHERE six_matches_played.match_id=$id " .
				"AND six_matches_played.home=0";
				$result2 = mysql_query($sql2);
				$row2 = mysql_fetch_array($result2);
				$player_away = $row2['name'];
				$nationalityAway = $row2['nationality'];
				$profileNameAway = $row2['profileName'];
				
        $player_away2 = "";
        $nationalityAway2 = "";
        $profileNameAway2 = "";

        $player_away3 = "";
        $nationalityAway3 = "";
        $profileNameAway3 = "";

				$row2 = mysql_fetch_array($result2);
				if (!empty($row2)) {
					$player_away2 = $row2['name'];
					$nationalityAway2 = $row2['nationality'];
					$profileNameAway2 = $row2['profileName'];
          $row2 = mysql_fetch_array($result2);
          if (!empty($row2)) {
            $player_away3 = $row2['name'];
            $nationalityAway3 = $row2['nationality'];
            $profileNameAway3 = $row2['profileName'];
          } 
				} 
        
				$rowheight = 19;
				$idtooltip = "style='cursor:help;' title='ID #$id'";
				
				if ($score_home < $score_away) {
					$scoreLeft = $score_away;
					$scoreRight = $score_home;
					$nationalityLeft = $nationalityAway;
					$nationalityRight = $nationalityHome;
          $ladderTeamLeft = $ladderTeamAway;
          $ladderTeamRight = $ladderTeamHome;
					$profileNameLeft = $profileNameAway;
					$profileNameRight = $profileNameHome;
					$playerLeft = $player_away;
					$playerRight = $player_home;

					$nationalityLeft2 = $nationalityAway2;
					$nationalityRight2 = $nationalityHome2;
          $profileNameLeft2 = $profileNameAway2;
					$profileNameRight2 = $profileNameHome2;
					$playerLeft2 = $player_away2;
					$playerRight2 = $player_home2;

					$nationalityLeft3 = $nationalityAway3;
					$nationalityRight3 = $nationalityHome3;
          $profileNameLeft3 = $profileNameAway3;
					$profileNameRight3 = $profileNameHome3;
					$playerLeft3 = $player_away3;
					$playerRight3 = $player_home3;
        } else {
					$scoreLeft = $score_home;
					$scoreRight = $score_away;
					$nationalityLeft = $nationalityHome;
					$nationalityRight = $nationalityAway;
          $ladderTeamLeft = $ladderTeamHome;
          $ladderTeamRight = $ladderTeamAway;
					$profileNameLeft = $profileNameHome;
					$profileNameRight = $profileNameAway;
					$playerLeft = $player_home;
					$playerRight = $player_away;
					
          $nationalityLeft2 = $nationalityHome2;
					$nationalityRight2 = $nationalityAway2;
					$profileNameLeft2 = $profileNameHome2;
					$profileNameRight2 = $profileNameAway2;
					$playerLeft2 = $player_home2;
					$playerRight2 = $player_away2;

          $nationalityLeft3 = $nationalityHome3;
					$nationalityRight3 = $nationalityAway3;
					$profileNameLeft3 = $profileNameHome3;
					$profileNameRight3 = $profileNameAway3;
					$playerLeft3 = $player_home3;
					$playerRight3 = $player_away3;
				}

				if (empty($playerLeft2)) {
					$winnerdisplay = '<span style="cursor:help" title="server profile" class="darkgrey-small">('.$profileNameLeft.')</span>&nbsp<span><a href="'.$directory.'/profile.php?name='.$playerLeft.'">'.$playerLeft.'</a></span>';
				} else {
					$winnerdisplay = '<span><a href="'.$directory.'/profile.php?name='.$playerLeft.'" title="server profile: '.$profileNameLeft.'">'.$playerLeft.'</a></span>'.
						'<span class="darkgrey-small">&ndash;</span>'.
						'<span><a href="'.$directory.'/profile.php?name='.$playerLeft2.'" title="server profile: '.$profileNameLeft2.'">'.$playerLeft2.'</a></span>';
          if (!empty($playerLeft3)) {
            $winnerdisplay .= '<span class="grey-small">&ndash;</span>'.
              '<span><a href="'.$directory.'/profile.php?name='.$playerLeft3.'" title="server profile: '.$profileNameLeft3.'">'.$playerLeft3.'</a></span>';
          }
        }
				if (empty($playerRight2)) {
					$loserdisplay = '<span><a href="'.$directory.'/profile.php?name='.$playerRight.'">'.$playerRight.'</a></span>'.
						'&nbsp;<span style="cursor:help" title="server profile" class="grey-small">('.$profileNameRight.')</span>';
				} else {
					$loserdisplay = '<span><a href="'.$directory.'/profile.php?name='.$playerRight.'" title="server profile: '.$profileNameRight.'">'.$playerRight.'</a></span>'.
						'<span class="grey-small">&ndash;</span>'.
						'<span><a href="'.$directory.'/profile.php?name='.$playerRight2.'" title="server profile: '.$profileNameRight2.'">'.$playerRight2.'</a></span>';
            if (!empty($playerRight3)) {
              $loserdisplay .= '<span class="grey-small">&ndash;</span>'.
                '<span><a href="'.$directory.'/profile.php?name='.$playerRight3.'" title="server profile: '.$profileNameRight3.'">'.$playerRight3.'</a></span>';
            }
				}
				
        if (is_null($ladderTeamLeft)) {
          $flagLeft = '<img title="'.$ladderTeamLeft.'" style="opacity:1;" src="'.$directory.'/flags/'.$nationalityLeft.'.bmp" width="18" height="15" border="0">';
        } else {
          $flagLeft = getImgForTeam($ladderTeamLeft);
        }
        
        if (is_null($ladderTeamRight)) {
          $flagRight = '<img title="'.$ladderTeamRight.'" style="opacity:1;" src="'.$directory.'/flags/'.$nationalityRight.'.bmp" width="18" height="15" border="0">';
        } else {
          $flagRight = getImgForTeam($ladderTeamRight);
        }
        
				$rowspan = "";

				?>
			<tr style='height:<?= $rowheight ?>px;'>

				<td nowrap style="padding-right: 5px;"><?= getImgForVersion('H') ?></td>
				<td <?= $idtooltip ?> nowrap style="padding-right: 15px;"><span class="darkgrey-small"><?= $gamedate ?></span></td>
				<td nowrap align="right"><?= $winnerdisplay ?></td>
				<td><?= $flagLeft ?></td>
				<td align="right" class="rightalign_gamesbox"><b><?= $scoreLeft ?></b></td>
				<td>-</td>
				<td><b><?= $scoreRight ?></b></td>
				<td><?= $flagRight ?></td>
				<td nowrap><?= $loserdisplay ?></td>
			</tr>

			<? } else { ?>
			<tr style='height:<?= $rowheight ?>px;'>
				<td colspan="6">(no game)</td>
			</tr>
			<? } ?>

			<?
			$compteur ++;
}
?>
		</table>
		<? echo getBoxBottom() ?></td>
		
		<td valign="top"><!-- standings --> <?
    
		$standingsLinksArray[] = array ('./standings.php', 'show_standings', 'show complete standings');

		$boxTitle = "Sixserver Standings";
		echo getBoxTopImg($boxTitle, $boxheight, false, $standingsLinksArray, $box2Img, $box2Align);
		?>
		<table>
		<?php

		$sql = "SELECT six_profiles.points, six_profiles.name as profileName, six_profiles.id AS profileId, six_profiles.rank, " .
			"weblm_players.name AS playerName, weblm_players.nationality, weblm_players.approved FROM six_profiles " .
			"LEFT JOIN weblm_players ON weblm_players.player_id=six_profiles.user_id " .
      "WHERE weblm_players.approved='yes' ".
			"ORDER BY six_profiles.points DESC, playerName ASC LIMIT 0,10";

		$result = mysql_query($sql);
		$num = mysql_num_rows($result);
		$compteur = 1;
		$col = "color:#555555";
		$oldPoints = -1;
		while (10 >= $compteur) {
			$row = mysql_fetch_array($result);

			if (!empty ($row["nationality"])) {
				$nationality = $row["nationality"];
			} else {
				$nationality = 'No country';
			}
			
			$name = $row['playerName'];
			$nameClass = colorNameClass($name, $row['approved']);
			$points = $row['points'];
			$profileId = $row['profileId'];
			$wins = GetSixserverWins($profileId);
			$losses = GetSixserverLosses($profileId);
			$draws = GetSixserverDraws($profileId);
			
			$pos = "";
			if ($oldPoints != $points) {
				$pos = $compteur;
				$oldPoints = $points;
			}
			
			echo "<tr style='height:19px;'>";
			if (!empty ($row)) {
				echo "<td style='text-align:right;cursor:hand;'>".$pos.".</td>";
				echo "<td><img src='$directory/flags/$nationality.bmp' width='18' height='15' border='0'></td>";
				echo "<td width='180' $nameClass style='white-space:nowrap;'><a href='$directory/profile.php?name=$name' title='view profile'>$name</a>&nbsp;&nbsp;<span style='cursor:help;' class='darkgrey-small' title='server profile'>(".$row['profileName'].")</span></td>";
				echo "<td align=\"right\" width='80'>";
				echo "<b>".$points."</b>&nbsp;&nbsp;<span style='$col'>points</span>&nbsp;&nbsp;";
				echo "</td>";
				echo "<td align=\"right\"><b>".$wins."</b><span style='$col'>&nbsp;W</span><td>";
				echo "<td align=\"right\"><b>".$draws."</b><span style='$col'>&nbsp;D</span><td>";
				echo "<td align=\"right\"><b>".$losses."</b><span style='$col'>&nbsp;L</span></td>";

			} else {
				echo '<td colspan="6"><span class="darkgrey-small">(no player)</span></td>';
			}
			echo "</tr>";

			$compteur++;
		}
		?>
		</table>
		<? echo getBoxBottom() ?></td>
	</tr>
	<tr>
    
    <td width="50%" valign="top">
    <? 
    $hasSixserverGamesToEdit = false;
    if ($loggedIn) {
      $sql = "SELECT COUNT(*) FROM $gamestable ".
        "WHERE sixGameId IS NOT NULL ".
        "AND edited=0 ".
        "AND deleted=0 ".
        "AND (winner='$cookie_name' OR loser='$cookie_name' OR winner2='cookie_name' OR loser2='$cookie_name') ".
        "AND season=".$season." ".
        "AND (winnerteam=0 OR loserteam=0)";
      $row = mysql_fetch_array(mysql_query($sql));
      $hasSixserverGamesToEdit = $row[0] > 0;
    }
    if ($hasSixserverGamesToEdit) {
    	include "./sixgamesForLadder.php";
    } else {
      $sql = "SELECT sm.id, sm.hashHome, spu.hash ".
        "FROM six_matches sm ".
        "LEFT JOIN six_patches sp1 ON sp1.hash = sm.hashHome ".
        "LEFT JOIN six_matches_played smp on smp.match_id=sm.id ".
        "LEFT JOIN six_profiles sp ON sp.id=smp.profile_id ".
        "LEFT JOIN weblm_players wp ON wp.player_id=sp.user_id ".
        "LEFT JOIN six_patches_unknown spu ON (spu.userId=sp.user_id AND sm.hashHome=spu.hash) ".
        "WHERE sm.hashHome = sm.HashAway ".
        "AND sm.hashHome <> '' ".
        "AND sp1.name IS NULL ".
        "AND spu.hash IS NULL ".
        "AND wp.name='".$cookie_name."' ".
        "ORDER BY sm.id DESC ".
        "LIMIT 0,1";
        
      if ($row = mysql_fetch_array(mysql_query($sql))) {
        $spMatchId = $row['id'];
        $spHash = $row['hashHome'];
        include "./sixpatch.php";
      } else {
        // $logRep = new KLogger('/var/www/yoursite/http/log/sixgamesDirect/', KLogger::INFO);	
        
        $sql = "SELECT
smp.match_id, st1.ladderTeamId AS ladderTeamIdHome, st2.ladderTeamId As ladderTeamIdAway, 
wt1.name AS ladderTeamNameHome, wt2.name AS ladderTeamNameAway, 
sm.score_home, sm.score_away, sm.team_id_home, sm.team_id_away, UNIX_TIMESTAMP(sm.played_on) as played_on, 
sp1.id as patchId
FROM six_matches_played smp 
LEFT JOIN six_matches sm ON sm.id=smp.match_id 
LEFT JOIN six_profiles sp ON sp.id=smp.profile_id
LEFT JOIN weblm_players wp ON wp.player_id=sp.user_id
LEFT JOIN six_patches sp1 ON sm.hashHome=sp1.hash
LEFT JOIN six_patches sp2 ON sm.hashAway=sp2.hash
LEFT JOIN six_teams st1 ON (st1.patchId=sp1.id AND st1.sixTeamId=sm.team_id_home) 
LEFT JOIN weblm_teams wt1 ON wt1.id=st1.ladderTeamId 
LEFT JOIN six_teams st2 ON (st2.patchId=sp2.id AND st2.sixTeamId=sm.team_id_away) 
LEFT JOIN weblm_teams wt2 ON wt2.id=st2.ladderTeamId 
WHERE wp.name='$cookie_name'
AND (st1.ladderTeamId IS NULL OR st2.ladderTeamId IS NULL) 
AND sm.reported=0
AND sm.edited=0 
AND sp1.hash=sp2.hash 
AND sp1.hash IS NOT NULL 
ORDER BY sm.id DESC";
        
        $result = mysql_query($sql);
        // $logRep->logInfo('sql='.$sql.' mysql_num_rows='.mysql_num_rows($result));
        if (mysql_num_rows($result) > 0) {
          include "./sixgamesDirect.php";
        } else {
    if ($isNew) {
    ?>
    
    <div id="newsbox" class="newsbox">
	    <?

				
		$newsLinksArray[] = array ('/news.php', 'show_news', 'show all news');
		$boxtitle = 'News';
		echo getBoxTopImg($boxtitle, $boxheight, $isNew, $newsLinksArray, $box3Img, $box3Align);
		
		?>
		<table class="layouttable">
			<tr class="row_newsheader">
				<td><?

				echo "<b>$date - $title</b>&nbsp;&nbsp;<span class='grey-small'>(posted by $user)</span>";
				?></td>
			</tr>
			<tr>
				<td><?php echo"$news" ?></td>
			</tr>
		</table>
		<? echo getBoxBottom() ?>
		</div>
    
    <?
    } else {
    ?>
      <div id="shoutbox" class="shoutbox">
      <?
      echo getBoxTopImg("Chatroom", $boxheight, false, null, $box3Img, $box3Align);
      include "./chatbox.php";
      echo getBoxBottom(); 
      ?>
      </div>
    <?  
          }
        } 
      } 
    } 
    ?>
	</td>
	
	<td valign="top">				<!-- joins -->
						<? 
						$joinLinksArray[] = array('./players.php', 'show_players', 'show all players');
						echo getBoxTop("New Players", $boxheight, false, $joinLinksArray); ?>
							  <table>
								<?
								   $sql="SELECT * FROM $playerstable where approved ='yes' ORDER BY joindate DESC LIMIT 0, 10";
								   $result = mysql_query($sql);
								   $compteur = 1;
								   while (10 >= $compteur) {
								     $row = mysql_fetch_array($result);
								
								     $name = $row["name"];
								     $alias = $row["alias"];
								     $platform = $row["platform"];
								     $approved = $row["approved"];
								     $nameClass = colorNameClass($name, $approved, $platform);
								     
								     if (!empty($row["nationality"])) {
								         $nationality = $row["nationality"];
								     } else {
								         $nationality = 'No country';
								     }
								     
								     $joined = formatDate($row['joindate']);
								     
								     echo "<tr>";
								     echo "<td nowrap><span class='size11'>".$joined."</span>&nbsp;</td>";
								     echo "<td nowrap><img src='$directory/flags/$nationality.bmp' border='0' title='$nationality'></td>";
								     if (!empty($alias)) {
								     	$showalias = " (aka $alias)";
								     } 
								     else {
								     	$showalias = "";
								     }
								     echo "<td $nameClass>&nbsp;<a href='$directory/profile.php?name=$name' title='view profile'>$name$showalias</a></td>";
								     echo "</tr>";
								
								     $compteur++;
								   }
								
								?>
								</table> 
						<? echo getBoxBottom() ?></td>
	</tr>
</table>
		<?
		$rssgif = '<img src="/gfx/rss.gif" style="vertical-align: middle" border="0">';
		$sitemapgif = '<img src="/gfx/sitemap.gif" style="vertical-align: middle" border="0">';
		$shopgif = '<img src="/gfx/shirt.gif" style="vertical-align: middle" border="0">';
		$fbimg = '<img src="/gfx/facebook.jpg" style="vertical-align: middle" border="0">';
		$linksBottomLeft = '<a style="vertical-align: middle" href="/sitemap.php">'.$sitemapgif.'&nbsp;Sitemap</a>&nbsp;&nbsp;';
		$linksBottomLeft .= '<a style="vertical-align: middle" target="_new" href="http://www.facebook.com/"'.$leaguename.'>'.$fbimg.'&nbsp;Facebook</a>&nbsp;&nbsp;';
		// $linksBottomRight .= '<a style="vertical-align: middle" href="/podcast/">'.$rssgif.' Podcast</a>&nbsp;';
		?>
		<?= getOuterBoxBottomLinks($linksBottomLeft, $linksBottomRight) ?>

		<?php
		require ('bottom.php');

?>

