#!/bin/bash

cd /tmp/
curl -O https://github.com/downloads/libevent/libevent/libevent-2.0.21-stable.tar.gz
tar -xzvf libevent-2.0.21-stable.tar.gz 
cd libevent-2.0.21-stable
./configure 
make && make install

cd ..

curl -O http://turnserver.open-sys.org/downloads/v3.2.3.8/turnserver-3.2.3.8.tar.gz
tar -xzvf turnserver-3.2.3.8.tar.gz 
cd turnserver-3.2.3.8
./configure 
make && make install

mkdir -p /etc/turnserver/

cp /usr/local/etc/turnserver.conf.default /etc/turnserver/turserver.conf
cp ./rpm/turnserver.init.el /etc/init.d/turnserver
ln -s /usr/local/bin/turnserver /usr/bin/turnserver
chmod +x /etc/init.d/turnserver 
useradd -s /sbin/nologin -M turnserver
service turnserver start
chkconfig turnserver on